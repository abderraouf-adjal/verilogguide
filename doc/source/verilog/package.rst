Packages
********


Introduction
============

In this chapter, we will see various features of SystemVerilog which is available for packages. We will see following synthesizable-features of SystemVerilog, 

* Packages : similar to packages in VHDL
* typedef : it is used to create the user-defined types
* struct : similar to 'structure' in C/C++


Also, following are the synthesizable constructs which can be used in Packages, 

* typedef
* struct
* import
* task
* functions
* const
* parameter, localparameter

Creating packages
=================

In :numref:`sv_mypackage`, a package 'my_package' is created in the file 'my_package.sv'.

.. code-block:: systemverilog
    :linenos:
    :caption: package 'my_package'
    :name: sv_mypackage

    // my_package.sv

    package my_package; // package name
        parameter initial_value = 1; // initial value
        typedef enum {ADD, SUB} op_list; // list of operatios

        typedef struct{
            logic [4:0] a, b; // for input
            logic [9:0] m; // for output : store multiplication result
        } port_t;

    endpackage 
     

     
Import package
==============

Package can be imported in following ways. The first three methods of import are shown in :numref:`sv_use_package` and the simulation results are shown in :numref:`fig_use_package`.

* Wild card import using \* (Line 4)
* Import specific item using 'import' and 'scope-resolution-operator \:\:' (Line 7)
* Direct import using \:\: (Line 11)
* Inport using \`include (discussed in :numref:`sec_cond_compilation`) 
  

.. note:: 

    * Wildcard import does not import items but adds the package into the path. 
    * Importing the 'enum-definition' will not import it's labels e.g. 'import op_list' will not import 'ADD' and 'SUB'.



.. code-block:: systemverilog
    :linenos:
    :caption: import package 'my_package'
    :name: sv_use_package

    // use_package.sv

    // // import method 1 : import everything from my_package
    // import my_package::*; 

    // // import method 2 : import individual value from package
    import my_package::initial_value; // import 'initial_value' from my_package

    module use_package(
        input logic clk,
        input my_package::port_t D, // import port_t from my_package
        output logic[5:0] s
    );

    always_ff @(posedge clk) begin
        s = D.a + D.b + initial_value; 
        D.m = D.a * D.b; 
    end

    endmodule


.. _`fig_use_package`:

.. figure:: figures/package/pack1.png

   Simulation results of :numref:`sv_use_package`


.. _`sec_cond_compilation`: 

Package with conditional compilation
====================================

In this section, we will create a testbench for 'my_package.sv' file; and use the keyword 'include' to use the package in it. 


Modify my_package.sv
--------------------

* In :numref:`sv_my_package_cond`, the wildcard import statement is added at the Line 17, which is not the part of the package 'my_package'.
* To import the Line 17, we need to use 'include' directive in the code as shown in Line 3 of :numref:`sv_use_package_tb`. 
* Lines 3-4 and 19 are the statement for conditional compilation. In the other words, if the 'my_package.sv' is not compiled/imported, then it will be complied/imported. 


.. note:: 

    Since, directive 'include' is not used in :numref:`sv_use_package` therefore Line 17 of :numref:`sv_my_package_cond` will not be executed in :numref:`sv_use_package`, therefore we need to use the package-name to import items e.g. my_package::port_t etc. 


.. code-block:: systemverilog
    :linenos:
    :emphasize-lines: 3-4, 16-17, 19
    :caption: Contditional compilation using 'ifndef'
    :name: sv_my_package_cond

    // my_package.sv

    `ifndef MYPACKAGE_DONE
        `define MYPACKAGE_DONE
        package my_package; // package name
            parameter initial_value = 1; // initial value
            typedef enum {ADD, SUB} op_list; // list of operatios

            typedef struct{
                logic [4:0] a, b; // for input
                logic [9:0] m; // for output : store multiplication result
            } port_t;

        endpackage 

        // import package in the design
        import my_package::*; 

    `endif

     


Testbench
---------

:numref:`sv_use_package_tb` is the testbench for :numref:`sv_use_package`. The results are shown in :numref:`fig_use_package_tb`. 

.. note:: 

    * \`timescale 1ns/10ps directive can be used in SystemVerilog. 
    * Also, we can further split \`timescale directive in SystemVerilog as below, 
    
    .. code-block:: systemverilog
    
        timeunit 1ns;
        timeprecision 10ps;
        
    * Lastly, if we defined the time-parameters in 'package' and 'design-file', then 'design-file parameters' will override the 'package-parameters'. 

.. code-block:: systemverilog
    :linenos:
    :emphasize-lines: 3, 8
    :caption: Testbench
    :name: sv_use_package_tb

    // use_package_tb.sv

    `include "my_package.sv"

    module use_package_tb;

    logic clk = 0; // initialize clock with 0
    port_t D; // import port_t from my_package
    logic[5:0] sum;

    use_package uut (
        .clk(clk), 
        .D(D),
        .s(sum)
    );

    always #10
        clk = ~clk;

    initial begin
        @(negedge clk)
        D.a = 6;
        D.b = 5;
    end
    endmodule


.. _`fig_use_package_tb`:

.. figure:: figures/package/pack2.png

   Simulation results of :numref:`sv_use_package_tb`
